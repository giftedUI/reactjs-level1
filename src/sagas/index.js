import { all, takeEvery } from 'redux-saga/effects';
import { MY_DEMO_ACTION } from '../app/tco-comparison-calculator/action';
import { myTestSaga } from '../app/tco-comparison-calculator/saga';

function* watchAPIRequests() {
    yield takeEvery(MY_DEMO_ACTION, myTestSaga)
}

export function* rootSaga() {
    yield all([
        watchAPIRequests()
    ]);
}
