import React from 'react';

const PageHeader = ({ heading, description }) => (
  <div className="page-header">
    <h2 className="page-title">{heading}</h2>
    <span>{description}</span>
  </div>
)

export default PageHeader;