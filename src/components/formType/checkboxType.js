import React from 'react';

class CheckboxTypeComponent extends React.Component {
  render() {
    return <div className="form-group">
      <label htmlFor={`input-type-${this.props.obj.type}`} className="col-form-label">{this.props.obj.label}</label>
      <input id={`input-type-${this.props.obj.type}`} type="checkbox" className="form-control" />
    </div>
  }
}

export default CheckboxTypeComponent;
