import React from 'react';

class RadioTypeComponent extends React.Component {
  render() {
    return <div className="form-check">
      <input id={`input-type-${this.props.obj.type}`} type="radio" className="form-check-input" />
      <label htmlFor={`input-type-${this.props.obj.type}`} className="form-check-label">{this.props.obj.label}</label>
    </div>
  }
}

export default RadioTypeComponent;
