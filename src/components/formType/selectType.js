import React from 'react';

class SelectTypeComponent extends React.Component {
  render() {
    const lists = this.props.obj.list;
    const options = [];
    lists.forEach((list, index) => {
      options.push(<option key={`${list}-${index}`} value={list}>{list}</option>)
    });
    return <div className="form-check">
      <label htmlFor={`input-type-${this.props.obj.type}`} className="form-check-label">{this.props.obj.label}</label>
      <select id={`input-type-${this.props.obj.type}`} className="custom-select">
        <option value="1">Select An Option</option>
        {options}
      </select>
    </div>
  }
}

export default SelectTypeComponent;
