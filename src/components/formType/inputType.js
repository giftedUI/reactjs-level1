import React from 'react';

class InputTypeComponent extends React.Component {
  render() {
    return <div className="form-group">
      <label htmlFor={`input-type-${this.props.obj.type}`} className="col-form-label">{this.props.obj.label}</label>
      <input id={`input-type-${this.props.obj.type}`} type="text" className="form-control" />
    </div>
  }
}

export default InputTypeComponent;
