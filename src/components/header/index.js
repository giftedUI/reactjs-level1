import React from 'react';
import logo from '../../static/imgs/logo.svg';
import Navbar from 'react-bootstrap/Navbar';

const Header = (props) => (
  <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="#home">
      <img
        src={logo}
        width="30"
        height="30"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
      {' Implement your own header'}
    </Navbar.Brand>
  </Navbar>
)

export default Header;