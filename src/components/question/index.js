import React from 'react';

const Question = ({ heading, description, children }) => (
  <section className="questions-section">
    <h4>{heading}</h4>
    <p>{description}</p>
    {children}
  </section>
)

export default Question;