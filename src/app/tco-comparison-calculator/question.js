export const QuestionList = {
  infrastructureType: {
    label: 'Are you...',
    value: '',
    type: 'special'
  },
  currency: {
    label: 'Currency',
    value: '',
    type: 'select',
    list: ['India', 'US', 'Australia']
  },
  vm: {
    label: 'How many Virtual Machine you want to deploy?',
    value: '',
    type: 'input',
    list: ['India', 'US', 'Australia']
  },
}