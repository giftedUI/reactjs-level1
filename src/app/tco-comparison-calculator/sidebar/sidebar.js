import React from 'react';

class SidebarComponent extends React.Component {
  render() {
    const lists = this.props.questions;
    const questionsArray = [];
    Object.keys(lists).forEach((key, index) => {
      questionsArray.push(
        <li
          key={`${lists[key]}-${index}`}
          className="list-group-item">
          <h5>{index + 1} {lists[key].label}</h5>
          <p>{ lists[key].value ? lists[key].value : 'None'}</p>
        </li>
      )
    });
    return <ul className="list-group">
      {questionsArray}
    </ul>
  }
}

export default SidebarComponent;
