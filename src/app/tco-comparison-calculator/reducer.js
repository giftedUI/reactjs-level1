import { MY_DEMO_ACTION } from './action';
import produce from 'immer';

const defaultState = {
  questions: []
};

export default function tcoComparisonCalculator(state = defaultState, action) {
  return produce(state, draft => {
    switch (action.type) {
      case MY_DEMO_ACTION:
        draft.questions = action.data;
        return;
      default:
        return;
    }
  });
}