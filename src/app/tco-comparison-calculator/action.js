export const MY_DEMO_ACTION = "MY_DEMO_ACTION";

export function myDemoAction() {
  return {
    type: MY_DEMO_ACTION
  }
}