const delay = (ms) => new Promise(res => setTimeout(res, ms))

export function* myTestSaga() {
  yield delay(1000)
  console.log('My test saga log!')
}