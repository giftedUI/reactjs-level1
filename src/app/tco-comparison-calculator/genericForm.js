import React from 'react';
import CheckboxTypeComponent from '../../components/formType/checkboxType';
import InputTypeComponent from '../../components/formType/inputType';
import RadioTypeComponent from '../../components/formType/radioType';
import SelectTypeComponent from '../../components/formType/selectType';
import SpecialTypeComponent from '../../components/formType/specialType';

class GenericFormComponent extends React.Component {
  render() {
    if (this.props.obj.type === 'input') {
      return <InputTypeComponent obj={this.props.obj} />
    } else if (this.props.obj.type === 'checkbox') {
      return <CheckboxTypeComponent obj={this.props.obj} />
    } else if (this.props.obj.type === 'radio') {
      return <RadioTypeComponent obj={this.props.obj} />
    } else if (this.props.obj.type === 'select') {
      return <SelectTypeComponent obj={this.props.obj} />
    } else if (this.props.obj.type === 'special') {
      return <SpecialTypeComponent obj={this.props.obj} />
    }
  }
}

export default GenericFormComponent;
