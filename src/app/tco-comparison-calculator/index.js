import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { connect } from 'react-redux';
import PageHeader from '../../components/page-heading';
import Question from '../../components/question';
import { myDemoAction } from './action';
import GenericFormComponent from './genericForm';
import { QuestionList } from './question';
import SidebarComponent from './sidebar/sidebar';

const TcoComparisonCalculator = (props) => {
  props.callSaga();
  const pageDescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ull';
  const items = [];

  Object.keys(QuestionList).forEach((key, index) => {
    items.push(
      <Question key={key} heading={`${index + 1} Question heading `}
        description="question desription in detail">
        <GenericFormComponent key={key} obj={QuestionList[key]} />
      </Question>
    )
  });
  return (
    <>
      <PageHeader heading="TCO Comparison Calculator"
        description={pageDescription} />
      <div className="questions-container">
        <Row>
          <Col md={8}>
            <form>{items}</form>
          </Col>
          <Col md={4}>
            <SidebarComponent questions={QuestionList} />
          </Col>
        </Row>
      </div>
    </>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    callSaga: () => dispatch(myDemoAction())
  };
};

const mapStateToProps = (state) => {
  return {
    questions: state.tcoComparisonCalculator.questions
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TcoComparisonCalculator);