import React from 'react';
import Container from 'react-bootstrap/Container';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import Header from '../components/header';
import './App.scss';
import { messages } from './messages';
import store from './redux-store';
import TcoComparisonCalculator from './tco-comparison-calculator';
import Row from 'react-bootstrap/Row';

function App() {
  return (
    <IntlProvider locale="en" messages={messages}>
      <Provider store={store}>
        <div className="App">
          <Header />
          <Container className="page-container">
            <TcoComparisonCalculator />
          </Container>
        </div>
      </Provider>
    </IntlProvider>
  );
}

export default App;
