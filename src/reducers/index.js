import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import tcoComparisonCalculator from '../app/tco-comparison-calculator/reducer'

const rootReducer = combineReducers({
	tcoComparisonCalculator,
	form: reduxFormReducer
});

export default rootReducer;